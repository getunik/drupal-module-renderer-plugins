<?php

namespace Drupal\renderer_plugins;

abstract class BaseRenderer
{
	protected $plugin;

	function __construct($plugin)
	{
		$this->plugin = $plugin;
	}

	public function render($node, $view_mode, $args, $entity_type)
	{
		$wrapper = entity_metadata_wrapper($entity_type, $node);
		try
		{
			$view_data = $this->execute($wrapper, $view_mode, $args);
			$view_data['wrapper'] = $wrapper;
			$view_data['renderer_plugin'] = $this->plugin;

			return theme('renderer_plugin', $view_data);
		}
		catch (Exception $e)
		{
			watchdog('renderer_plugin', $e->getMessage(), NULL, WATCHDOG_ERROR);
			return '';
		}
	}

	public abstract function execute($wrapper, $view_mode, $args);
}
