<?php

function renderer_plugins_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state)
{
	return call_user_func('_renderer_plugins_' . $instance['display'][$view_mode]['type'] . '_settings_form', $field, $instance, $view_mode, $form, $form_state);
}

function renderer_plugins_field_formatter_settings_summary($field, $instance, $view_mode)
{
	return call_user_func('_renderer_plugins_' . $instance['display'][$view_mode]['type'] . '_settings_summary', $field, $instance, $view_mode);
}

function renderer_plugins_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display)
{
	return call_user_func('_renderer_plugins_' . $display['type'] . '_view', $entity_type, $entity, $field, $instance, $langcode, $items, $display);
}

function _get_args_from_doublefield($field_value)
{
	$args = array();
	$matches = NULL;

	foreach ($field_value as $delta => $argument)
	{
		if (preg_match('/(.*)\[(\d+)\]$/', $argument['first'], $matches))
		{
			if (!isset($args[$matches[1]]) || !is_array($args[$matches[1]]))
			{
				$args[$matches[1]] = array();
			}

			$args[$matches[1]][(int)$matches[2]] = $argument['second'];
		}
		else
		{
			$args[$argument['first']] = $argument['second'];
		}
	}

	return $args;
}

function _get_args_from_field($field_value)
{
	$args = array();

	foreach ($field_value as $delta => $argument)
	{
		if (isset($argument['value']))
		{
			$args[] = $argument['value'];
		}
	}

	return $args;
}

/**
* Gets the field values from an entity reference (currenty tested with paragraphs) and returns them as an array
*
* @param string $wrapper
* @param string $language
* @param string $field_prefix
* @return array $result
*
*/

function _get_args_from_entity_ref($wrapper, $language, $field_prefix)
{
	$result = array();

	$prefix = "field_" . ($field_prefix ? $field_prefix . "_" : '');

	foreach (array_keys($wrapper->getPropertyInfo()) as $name)
	{
		if (strpos($name, $prefix) === 0) {
			$new_name = str_replace($prefix, "", $name);
			//Special handling of link field
			if ($wrapper->{$name}->type() === "field_item_link")
			{
				$result[$new_name] = ($wrapper->{$name}->value()['url'] !== NULL) ? $wrapper->{$name}->value()['url'] : "";
			}
			else {
				$result[$new_name] = ($wrapper->{$name}->value() !== NULL) ? $wrapper->{$name}->value() : "";
			}
		}
	}

	return $result;
}

////////////////////////////
// renderer_plugin_formatter

function _renderer_plugins_renderer_plugin_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state)
{
	$display = $instance['display'][$view_mode];
	// This gets the actual settings
	$settings = $display['settings'];
	// Initialize the element variable
	$element = array();
	// Add class box
	$element['args_field'] = array(
		'#type'           => 'textfield',                        // Use a textbox
		'#title'          => t('Arguments Field'),                      // Widget label
		'#description'    => t('Name of the field containing the renderer arguments'),  // helper text
		'#default_value'  => $settings['args_field'],               // Get the value if it's already been set
	);
	$element['view_mode'] = array(
		'#type' => 'hidden',
		'#value' => $view_mode,
	);

	return $element;
}

function _renderer_plugins_renderer_plugin_formatter_settings_summary($field, $instance, $view_mode)
{
	// This gets the view_mode where our settings are stored
	$display = $instance['display'][$view_mode];
	// This gets the actual settings
	$settings = $display['settings'];
	// create the summary text from our settings
	$summary = t('Renderer plugin with arguments from "@args_field"', array(
		'@args_field'     => $settings['args_field'],
	));

	return $summary;
}

function _renderer_plugins_renderer_plugin_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display)
{
	$element = array(); // Initialize the var
	$settings = $display['settings']; // get the settings
	$args_field = $settings['args_field']; // The class setting selected in the settings form

	// extract arguments from field
	$args = array();
	$wrapper = entity_metadata_wrapper('node', $entity);
	if (isset($wrapper->{$args_field}))
	{
		$args = _get_args_from_doublefield($wrapper->{$args_field}->value());
	}

	// every delta in the field gets its theme settings
	foreach ($items as $delta => $item)
	{
		$element[$delta] = array(
			'#theme' => 'renderer_plugin_factory',
			'#renderer_plugin' => $item['value'],
			'#arguments' => $args,
			'#node' => $entity,
			'#view_mode' => $settings['view_mode'],
			'#entity_type' => $entity_type,
		);
	}

	return $element;
}

/////////////////////////////////
// renderer_plugin_args_formatter

function _renderer_plugins_renderer_plugin_args_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state)
{
	$display = $instance['display'][$view_mode];
	// This gets the actual settings
	$settings = $display['settings'];
	// Initialize the element variable
	$element = array();
	$element['renderer_plugin'] = array(
		'#type'           => 'textfield',                        // Use a textbox
		'#title'          => t('Renderer Plugin Name'),                      // Widget label
		'#description'    => t('Name of the renderer plugin to use'),  // helper text
		'#default_value'  => $settings['renderer_plugin'],               // Get the value if it's already been set
	);
	$element['view_mode'] = array(
		'#type' => 'hidden',
		'#value' => $view_mode,
	);

	return $element;
}

function _renderer_plugins_renderer_plugin_args_formatter_settings_summary($field, $instance, $view_mode)
{
	// This gets the view_mode where our settings are stored
	$display = $instance['display'][$view_mode];
	// This gets the actual settings
	$settings = $display['settings'];
	// create the summary text from our settings
	$summary = t('Renderer plugin "@name" with arguments from this field', array('@name' => $settings['renderer_plugin']));

	return $summary;
}

function _renderer_plugins_renderer_plugin_args_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display)
{
	$element = array(); // Initialize the var
	$settings = $display['settings']; // get the settings

	// extract arguments from field
	$args = array();
	if ($field['type'] === 'double_field')
	{
		$args = _get_args_from_doublefield($items);
	}
	else
	{
		$args = _get_args_from_field($items);
	}

	$element[] = array(
		'#theme' => 'renderer_plugin_factory',
		'#renderer_plugin' => $settings['renderer_plugin'],
		'#arguments' => $args,
		'#node' => $entity,
		'#view_mode' => $settings['view_mode'],
		'#entity_type' => $entity_type,
	);

	return $element;
}

/////////////////////////////////
// renderer_plugin_entity_ref_formatter

function _renderer_plugins_renderer_plugin_entity_ref_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state)
{
	$display = $instance['display'][$view_mode];
	// This gets the actual settings
	$settings = $display['settings'];
	// Initialize the element variable
	$element = array();
	$element['renderer_plugin'] = array(
		'#type'           => 'textfield',                        // Use a textbox
		'#title'          => t('Renderer Plugin Name'),                      // Widget label
		'#description'    => t('Name of the renderer plugin to use'),  // helper text
		'#default_value'  => $settings['renderer_plugin'],               // Get the value if it's already been set
	);

	$element['field_prefix'] = array(
		'#type'           => 'textfield',                        // Use a textbox
		'#title'          => t('Field prefix'),                      // Widget label
		'#description'    => t('The prefix of the field. Use ss for social sharing'),  // helper text
		'#default_value'  => $settings['field_prefix'],               // Get the value if it's already been set
	);

	$element['entity_type'] = array(
		'#type'           => 'textfield',                        // Use a textbox
		'#title'          => t('Entity Type Name'),                      // Widget label
		'#description'    => t('Name of the entity type this field is referencing'),  // helper text
		'#default_value'  => $settings['entity_type'],               // Get the value if it's already been set
	);

	$element['view_mode'] = array(
		'#type' => 'hidden',
		'#value' => $view_mode,
	);

	return $element;
}

function _renderer_plugins_renderer_plugin_entity_ref_formatter_settings_summary($field, $instance, $view_mode)
{
	// This gets the view_mode where our settings are stored
	$display = $instance['display'][$view_mode];
	// This gets the actual settings
	$settings = $display['settings'];
	// create the summary text from our settings
	$summary = t('Renderer plugin "@name" with arguments from this field', array('@name' => $settings['renderer_plugin']));

	return $summary;
}

function _renderer_plugins_renderer_plugin_entity_ref_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display)
{
	$element = array(); // Initialize the var
	$settings = $display['settings']; // get the settings

	ctools_include('plugins');
	$plugin = ctools_get_plugins('renderer_plugins', 'renderer', $settings['renderer_plugin']);

	//use the field prefix from the settings form if available, else take the prefix from the plugin settings
	$prefix = isset($settings['field_prefix']) ? $settings['field_prefix'] : $plugin['field_prefix'];

	// every delta in the field gets its theme settings
	foreach ($items as $delta => $item)
	{
		$target = "";
		switch ($field['type']) {
			case "paragraphs" :
				$target = $item['value'];
			break;

			case "entityreference" :
				$target = $item['target_id'];
			break;

			default:
				throw new Exception('Unknown field reference type. The formatter currently supports "paragraphs" and "entityreference"');
		}
		$entities = entity_load($display['settings']['entity_type'], array($target));

		if (count($entities) > 0)
		{
			$temp = array_values($entities);
			$entity = array_shift($temp);

			$wrapper = entity_metadata_wrapper($display['settings']['entity_type'], $entity, array('langcode' => $langcode));
			$args = _get_args_from_entity_ref($wrapper, $langcode,  $prefix);

			$element[$delta] = array(
				'#theme' => 'renderer_plugin_factory',
				'#renderer_plugin' => $settings['renderer_plugin'],
				'#arguments' => $args,
				'#node' => $entity,
				'#view_mode' => $settings['view_mode'],
				'#entity_type' => $display['settings']['entity_type']
			);
		}
	}

	return $element;
}
